package transparent.annotations;

public @interface MethodType
{
	public enum Type
	{
		SETTER,
		GETTER,
		UTILITY,
		NONE,
	}

	public Type value();
}
