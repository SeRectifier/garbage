package transparent.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;

import transparent.run.*;
import transparent.gui.doublebuffering.*;

public final class Master extends Frame
{
	private static final long serialVersionUID = 0x0L;

	private final Display display = new Display();

	private final Label labelFileName = new Label("No file is opened.");
	private final Button buttonLoad = new Button("Load");
	private final Button buttonSave = new Button("Save");
	private final Button buttonQuit = new Button("Quit");

	private RenderingDaemon daemon = new RenderingDaemon(this.display,30);

	public Master()
	{
		super("title");

		this.initListener();
		this.initPane();

		this.reflectChanges();

		super.setSize(141*5,100*5);
		super.setResizable(true);
	}

	@Override
	public void setVisible(boolean b)
	{
		super.setVisible(b);
		if(b) {
			new Thread(this.daemon).start();
		}
		else {
			this.daemon.stopRendering();
		}
	}

	private void initListener()
	{
		SelectionListener listenerSelection = new SelectionListener();
		FileListener listenerFile = new FileListener();
		QuitListener listenerQuit = new QuitListener();

		this.buttonLoad.addActionListener(listenerFile);
		this.buttonSave.addActionListener(listenerFile);
		this.buttonQuit.addActionListener(listenerQuit);
		this.display.addMouseListener(listenerSelection);
		super.addWindowListener(listenerQuit);
	}

	private void initPane()
	{
		JPanel paneSouth = new JPanel(new FlowLayout());
		paneSouth.add(this.buttonLoad);
		paneSouth.add(this.buttonSave);
		paneSouth.add(this.buttonQuit);

		super.add(this.labelFileName,BorderLayout.NORTH);
		super.add(this.display,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);
	}

	public void reflectChanges()
	{
		String nameFile = DataHolder.fileCurrent == null ? "No file is loaded." : DataHolder.fileCurrent.getAbsolutePath();
		this.labelFileName.setText(nameFile);
		this.buttonSave.setEnabled(DataHolder.imageEditing != null);
	}

	class SelectionListener implements MouseListener
	{
		@Override
		public void mouseClicked(MouseEvent e)
		{
			Point p = e.getPoint();
			int xImage = Master.this.display.getXCoordinateOnImage(p.x);
			int yImage = Master.this.display.getYCoordinateOnImage(p.y);

			if(xImage == -1 || yImage == -1) return;

			int rgbTarget = DataHolder.imageEditing.getRGB(xImage,yImage);

			int widthImage = DataHolder.imageEditing.getWidth();
			int heightImage = DataHolder.imageEditing.getHeight();

			for(int x=0; x<widthImage; x++) {
			for(int y=0; y<heightImage; y++) {
				if(DataHolder.imageEditing.getRGB(x,y) == rgbTarget) 
					DataHolder.imageEditing.setRGB(x,y,0x00ffffff);
			}}
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
			
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
	}

	class FileListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(Master.this.buttonLoad)) {
				FilenameFilter filter = new FilenameFilter() {
					@Override
					public boolean accept(File dir,String name)
					{
						return name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".bmp") || name.endsWith(".gif") || name.endsWith(".tiff");
					}
				};
				FileDialog dialogFile = new FileDialog(Master.this);
				dialogFile.setFilenameFilter(filter);
				dialogFile.setMode(FileDialog.LOAD);

				dialogFile.setVisible(true);
				String nameSelected = dialogFile.getFile();
				if(nameSelected == null) return;
				File fileSelected = new File(dialogFile.getDirectory(),nameSelected);

				try {
					BufferedImage image = ImageIO.read(fileSelected);
					DataHolder.imageOriginal = image;
					DataHolder.fileCurrent = fileSelected;

					int width = image.getWidth();
					int height = image.getHeight();
					DataHolder.imageEditing = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
					for(int x=0; x<width; x++) {
					for(int y=0; y<height; y++) {
						DataHolder.imageEditing.setRGB(x,y,image.getRGB(x,y));
					}}
				}
				catch(IOException ioe) {
					ioe.printStackTrace();
				}
				finally {
					Master.this.reflectChanges();
				}
			}

			if(src.equals(Master.this.buttonSave)) {
				FilenameFilter filter = new FilenameFilter() {
					@Override
					public boolean accept(File dir,String name)
					{
						return name.endsWith(".png") || name.endsWith(".bmp") || name.endsWith(".gif") || name.endsWith(".tiff");
					}
				};

				FileDialog dialogFile = new FileDialog(Master.this);
				dialogFile.setFilenameFilter(filter);
				dialogFile.setMode(FileDialog.SAVE);
				dialogFile.setFile(DataHolder.fileCurrent.toString());

				dialogFile.setVisible(true);
				String nameSelected = dialogFile.getFile();
				if(nameSelected == null) return;
				DataHolder.fileCurrent = new File(dialogFile.getDirectory(),nameSelected);

				try {
					String nameFile = DataHolder.fileCurrent.getName();
					String[] arraySplit = nameFile.split("\\.");
					String extension = (arraySplit.length == 0) ? "png" : arraySplit[arraySplit.length-1];

					ImageIO.write(
							DataHolder.imageEditing,
							extension,
							DataHolder.fileCurrent
					);

					Master.this.reflectChanges();
				}
				catch(IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	class QuitListener extends WindowAdapter implements ActionListener
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			System.exit(0);
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}
}
