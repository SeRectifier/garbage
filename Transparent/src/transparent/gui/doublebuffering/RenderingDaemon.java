package transparent.gui.doublebuffering;

import java.awt.*;
import java.awt.image.*;

import transparent.annotations.*;
import transparent.annotations.MethodType.*;

public class RenderingDaemon implements Runnable
{
	private Canvas target = null;
	private boolean launch = false;
	private long msInterval = 1000 / 30;

	public RenderingDaemon(Canvas t,int fps)
	{
		if(t == null) throw new IllegalArgumentException("Null canvas");
		this.target = t;
		this.target.setIgnoreRepaint(true);
	}

	@MethodType(Type.SETTER)
	public void setFPS(int fps)
	{
		this.msInterval = fps < 0 ? 1000 / 30 : 1000 / fps;
	}

	@Override
	public void run()
	{
		this.target.createBufferStrategy(2);
		BufferStrategy strategy = this.target.getBufferStrategy();
		Graphics g = null;

		this.launch = true;
		while(this.launch) {
			if(strategy.contentsLost()) continue;

			g = strategy.getDrawGraphics();
			this.target.paint(g);
			g.dispose();
			strategy.show();

			try {
				Thread.sleep(msInterval);
			}
			catch(InterruptedException ie) {}
		}
	}

	public void stopRendering()
	{
		this.launch = false;
	}
}
