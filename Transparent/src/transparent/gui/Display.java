package transparent.gui;

import java.awt.*;
import java.awt.image.*;
import transparent.annotations.*;
import transparent.annotations.MethodType.Type;
import transparent.run.DataHolder;

public final class Display extends Canvas
{
	private static final long serialVersionUID = 0x0L;

	private int marginMin = 32;

	private int marginX = 0;
	private int marginY = 0;
	private int widthCanvas = 0;
	private int heightCanvas = 0;
	private int widthRender = 0;
	private int heightRender = 0;

	private float ratioCanvas = 0.0f;
	private float ratioImage = 0.0f;

	private int widthImage = 0;
	private int heightImage = 0;

	private Color colorBright = new Color(255,255,255);
	private Color colorDark = new Color(223,223,223);
	private int sizeGrid = 12;
	private int numGridX = 0;
	private int numGridY = 0;

	@Override
	public void paint(Graphics g)
	{
		if(DataHolder.imageEditing == null) return;

		this.widthImage = DataHolder.imageEditing.getWidth();
		this.heightImage = DataHolder.imageEditing.getHeight();
		this.ratioImage = (float) this.heightImage / (float) this.widthImage;

		this.widthCanvas = super.getWidth();
		this.heightCanvas = super.getHeight();
		this.ratioCanvas = (float) this.heightCanvas / (float) this.widthCanvas;

		if(this.ratioImage < this.ratioCanvas) {
			this.marginX = this.marginMin;
			this.widthRender = this.widthCanvas - this.marginX  * 2;
			this.heightRender = Math.round(this.widthRender * this.ratioImage);
			this.marginY = Math.round((this.heightCanvas - this.heightRender) / 2.0f);
		}
		else {
			this.marginY = this.marginMin;
			this.heightRender = this.heightCanvas - this.marginY * 2;
			this.widthRender = Math.round(this.heightRender / this.ratioImage);
			this.marginX = Math.round((this.widthCanvas - this.widthRender) / 2.0f);
		}

		this.numGridX = this.widthRender / this.sizeGrid;
		this.numGridY = this.heightRender / this.sizeGrid;

		g.clearRect(0,0,this.widthCanvas,this.heightCanvas);

		for(int x=0; x<this.numGridX; x++) {
		for(int y=0; y<this.numGridY; y++) {
			if((x+y) % 2 == 0) g.setColor(this.colorBright);
			else g.setColor(this.colorDark);
			g.fillRect(
					this.marginX+this.sizeGrid*x,
					this.marginY+this.sizeGrid*y,
					this.sizeGrid,
					this.sizeGrid
			);
		}}

		g.drawImage(DataHolder.imageEditing,this.marginX,this.marginY,this.widthRender,this.heightRender,this);
	}

	@MethodType(Type.SETTER)
	public void setMinMargin(int m)
	{
		this.marginMin = (m < 0) ? 0 : m;
	}

	@MethodType(Type.SETTER)
	public void setOriginalImage(BufferedImage image)
	{
		DataHolder.imageEditing = this.cloneImage(image);

		this.widthImage = (DataHolder.imageEditing != null) ? DataHolder.imageEditing.getWidth() : 0;
		this.heightImage = (DataHolder.imageEditing != null) ? DataHolder.imageEditing.getHeight() : 0;
		this.ratioImage = (DataHolder.imageEditing != null) ? (float) this.heightImage / (float) this.widthImage : 0.0f;
	}

	@MethodType(Type.GETTER)
	public int getMinMargin()
	{
		return this.marginMin;
	}

	@MethodType(Type.UTILITY)
	public int getXCoordinateOnImage(int xDisplay)
	{
		int xOffset = xDisplay - this.marginX;
		if(xOffset < 0 || this.widthRender < xOffset || DataHolder.imageEditing == null) return -1;

		float ratioZoom = (float) this.widthRender / (float) this.widthImage;
		return Math.round((float) xOffset / ratioZoom);
	}

	@MethodType(Type.UTILITY)
	public int getYCoordinateOnImage(int yDisplay)
	{
		int yOffset = yDisplay - this.marginY;
		if(yOffset < 0 || this.heightRender < yOffset || DataHolder.imageEditing == null) return -1;

		float ratioZoom = (float) this.heightRender / (float) this.heightImage;
		return Math.round((float) yOffset / ratioZoom);
	}

	@MethodType(Type.UTILITY)
	public Point getCoordinateOnImage(Point pDisplay)
	{
		Point coi = new Point(
				this.getXCoordinateOnImage(pDisplay.x),
				this.getYCoordinateOnImage(pDisplay.y)
		);

		return coi;
	}

	private BufferedImage cloneImage(BufferedImage image)
	{
		if(image == null) return null;

		int width = image.getWidth();
		int height = image.getHeight();
		BufferedImage clone = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);

		for(int x=0; x<width; x++) {
		for(int y=0; y<height; y++) {
			clone.setRGB(x,y,image.getRGB(x,y));
		}}

		return clone;
	}
}
