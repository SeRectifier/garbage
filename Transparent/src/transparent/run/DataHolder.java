package transparent.run;

import java.awt.image.*;
import java.io.*;

public final class DataHolder
{
	public static File fileCurrent = null;
	public static BufferedImage imageOriginal = null;
	public static BufferedImage imageEditing = null;
}
