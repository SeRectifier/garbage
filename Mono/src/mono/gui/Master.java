package mono.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;

import mono.core.*;
import mono.core.data.*;

public final class Master extends Frame
{
	private static final long serialVersionUID = 0x0L;

	private final TextArea areaLogging = new TextArea();
	private final Button buttonLoad = new Button("Load");
	private final Button buttonPreprocess = new Button("Pre process");
	private final Button buttonAct = new Button("Start processing");
	private final Button buttonQuit = new Button("Quit");

	private int sizeMaterial = 32;

	private File dirWorking = null;
	private File[] arrayMaterialFiles = null;
	private BufferedImage imageTarget = null;

	public Master()
	{
		super("Monochrome mosaic");

		this.initField();
		this.initListener();
		this.initPane();

		super.setSize(707,500);
		super.setResizable(true);
	}

	public void logBlank()
	{
		this.areaLogging.append(System.lineSeparator());
	}

	public void log(String t)
	{
		this.areaLogging.append(t + System.lineSeparator());
	}

	public void print(String t)
	{
		this.areaLogging.append(t);
	}

	private boolean checkDirectory(File dir)
	{
		this.log("Checking \"" + dir.getAbsolutePath() + "\"...");

		if(!dir.exists()) {
			this.log("Directory does not exist.");
			this.logBlank();
			return false;
		}
		if(dir.isFile()) {
			this.log(dir.getAbsolutePath() + " is not directory.");
			this.logBlank();
			return false;
		}

		File fileTarget = new File(dir,"target.png");
		if(!fileTarget.exists()) {
			this.log("There is no \"target.png\" file in " + dir.getAbsolutePath() + ".");
			this.logBlank();
			return false;
		}

		File dirMaterials = new File(dir,"materials");
		if(!dirMaterials.exists()) {
			this.log("There is no \"materials\" folder in " + dir.getAbsolutePath() + ".");
			this.logBlank();
			return false;
		}
		if(dirMaterials.isFile()) {
			this.log(dirMaterials.getAbsolutePath() + " is not directory.");
			this.logBlank();
			return false;
		}

		File[] arrayTMF = dirMaterials.listFiles();
		String fn = null;
		ArrayList<File> listImageFile = new ArrayList<File>(0);
		for(int i=0; i<arrayTMF.length; i++) {
			fn = arrayTMF[i].getName();
			if(fn.endsWith(".png") || fn.endsWith(".jpg") || fn.endsWith(".bmp")) listImageFile.add(arrayTMF[i]);
		}

		this.log("The \"materials\" folder contains " + listImageFile.size() + " available image file(s).");

		try {
			BufferedImage imgTgt = ImageIO.read(fileTarget);
			long noe = imgTgt.getWidth() * imgTgt.getHeight();
			if(listImageFile.size() < noe) {
				this.log("There are too small amount of image files to generate mosaic image.");
				this.log("Please add " + (noe - listImageFile.size()) + " file(s).");
				this.logBlank();
				return false;
			}

			this.arrayMaterialFiles = new File[listImageFile.size()];
			for(int i=0; i<listImageFile.size(); i++) 
				this.arrayMaterialFiles[i] = listImageFile.get(i);

			this.imageTarget = imgTgt;
			this.dirWorking = dir;
		}
		catch(IOException ioe) {
			this.log("An error occured during the check phase of \"target.png\".");
			this.logBlank();
			return false;
		}

		this.log("The directory passed all check.");
		this.log(this.dirWorking.getAbsolutePath() + " is successfully loaded.");
		this.logBlank();

		return true;
	}

	private void process()
	{
		this.buttonAct.setEnabled(false);
		this.buttonLoad.setEnabled(false);
		new Thread(new Processor2()).start();
	}

	private void initField()
	{
		this.areaLogging.setEditable(false);
	}

	private void initListener()
	{
		FunctionListener listenerFunction = new FunctionListener();
		this.buttonAct.addActionListener(listenerFunction);
		this.buttonLoad.addActionListener(listenerFunction);

		QuitListener listenerQuit = new QuitListener();
		this.buttonQuit.addActionListener(listenerQuit);
		super.addWindowListener(listenerQuit);
	}

	private void initPane()
	{
		Panel paneSouth = new Panel(new FlowLayout());
		paneSouth.add(this.buttonLoad);
		paneSouth.add(this.buttonPreprocess);
		paneSouth.add(this.buttonAct);
		paneSouth.add(this.buttonQuit);

		super.add(this.areaLogging,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);
	}

	class Processor1 implements Runnable
	{
		@Override
		public void run()
		{
			File dirSquare = new File(Master.this.dirWorking,"square");
			dirSquare.mkdir();

			PrintWriter writer = null;
			try {
				writer = new PrintWriter(new FileWriter(new File(Master.this.dirWorking,"score.txt")));
			}
			catch(IOException ioe) {}

			ArrayList<Monochrome> listMonochrome = new ArrayList<Monochrome>();

			File fileCurrent = null;
			BufferedImage imageTemporary = null;
			Monochrome monochrome = null;
			for(int i=0; i<Master.this.arrayMaterialFiles.length; i++) {
				fileCurrent = Master.this.arrayMaterialFiles[i];
				try {
					imageTemporary = ImageIO.read(fileCurrent);
					imageTemporary = Converter.makeSquare(imageTemporary,Master.this.sizeMaterial);
					monochrome = new Monochrome(imageTemporary);
					listMonochrome.add(monochrome);
					ImageIO.write(monochrome.toImage(),"png",new File(dirSquare,"monochrome-" + i + ".png"));
					writer.println("Original:" + fileCurrent.getName());
					writer.println("\t" + "Destination:monochrome-" + i + ".png");
					writer.println("\t" + "Score:" + monochrome.getAverage());
				}
				catch(IOException ioe) {}
			}
		}
	}

	class Processor2 implements Runnable
	{
		@Override
		public void run()
		{
			File dirSquare = new File(Master.this.dirWorking,"square");
			dirSquare.mkdir();

			PrintWriter writer = null;
			try {
				writer = new PrintWriter(new FileWriter(new File(Master.this.dirWorking,"score.txt")));
			}
			catch(IOException ioe) {}

			ArrayList<Monochrome> listMonochrome = new ArrayList<Monochrome>(0);

			File fileCurrent = null;
			BufferedImage imageTemporary = null;
			Monochrome monochrome = null;
			for(int i=0; i<Master.this.arrayMaterialFiles.length; i++) {
				fileCurrent = Master.this.arrayMaterialFiles[i];
				try {
					imageTemporary = ImageIO.read(fileCurrent);
					imageTemporary = Converter.makeSquare(imageTemporary,Master.this.sizeMaterial);
					monochrome = new Monochrome(imageTemporary);
					listMonochrome.add(monochrome);
					ImageIO.write(monochrome.toImage(),"png",new File(dirSquare,"monochrome-" + i + ".png"));
					writer.println("Original:" + fileCurrent.getName());
					writer.println(i + "," + monochrome.getAverage());
				}
				catch(IOException ioe) {}
			}

			writer.close();

			int argb = 0;
			int average = 0;
			ArrayList<ScoredCoordinate> listCoordinate = new ArrayList<ScoredCoordinate>();

			int widthTarget = Master.this.imageTarget.getWidth();
			int heightTarget = Master.this.imageTarget.getHeight();
			for(int x=0; x<widthTarget; x++) {
			for(int y=0; y<heightTarget; y++) {
				argb = Master.this.imageTarget.getRGB(x,y);
				average = Math.round((((argb >> 16) & 0xff) + ((argb >> 8) & 0xff) + (argb & 0xff)) / 3.0f);
				listCoordinate.add(new ScoredCoordinate(x,y,average));
			}}

			BufferedImage product = new BufferedImage(
					widthTarget * Master.this.sizeMaterial,
					heightTarget * Master.this.sizeMaterial,
					BufferedImage.TYPE_INT_ARGB
			);

			Collections.sort(listMonochrome);
			Collections.sort(listCoordinate);

			ScoredCoordinate coordinate = null;
			BufferedImage img = null;
			for(int i=0; i<listCoordinate.size(); i++) {
				coordinate = listCoordinate.get(i);
				img = listMonochrome.get(i).toImage();
				for(int x=0; x<Master.this.sizeMaterial; x++) {
				for(int y=0; y<Master.this.sizeMaterial; y++) {
					product.setRGB(
							Master.this.sizeMaterial * coordinate.x + x,
							Master.this.sizeMaterial * coordinate.y + y,
							img.getRGB(x,y)
					);
				}}
			}

			try {
				ImageIO.write(product,"png",new File(Master.this.dirWorking,"product.png"));
			}
			catch(IOException ioe) {}

			Master.this.buttonAct.setEnabled(true);
			Master.this.buttonLoad.setEnabled(true);
		}
	}

	class FunctionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Object src = e.getSource();

			if(src.equals(Master.this.buttonAct)) {
				if(Master.this.dirWorking == null) {
					Master.this.log("No directory is loaded.");
					Master.this.logBlank();
					return;
				}

				Master.this.process();
			}

			if(src.equals(Master.this.buttonLoad)) {
				LoadDialog dialogLoad = new LoadDialog(Master.this);
				dialogLoad.setVisible(true);
				String result = dialogLoad.getResult();
				if(result == null) return;

				File temp = new File(result);
				Master.this.checkDirectory(temp);
			}
		}
	}

	class QuitListener extends WindowAdapter implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}

		@Override
		public void windowClosing(WindowEvent e)
		{
			System.exit(0);
		}
	}
}
