package mono.gui;

import java.awt.*;
import java.awt.event.*;

public final class LoadDialog extends Dialog
{
	private static final long serialVersionUID = 0x0L;

	private final Label labelPath = new Label("Path: ");
	private final TextField fieldPath = new TextField(32);
	private final Button buttonDone = new Button("Done");

	private String result = null;

	public LoadDialog(Frame frame)
	{
		super(frame,"Load",true);

		this.initListener();
		this.initPane();

		super.pack();
		super.setResizable(false);
	}

	public String getResult()
	{
		return this.result;
	}

	private void initListener()
	{
		Listener listener = new Listener();
		this.buttonDone.addActionListener(listener);
		super.addWindowListener(listener);
	}

	private void initPane()
	{
		GridBagLayout layoutCenter = new GridBagLayout();
		GridBagConstraints gbcCenter = new GridBagConstraints();
		Panel paneCenter = new Panel(layoutCenter);

		gbcCenter.gridx = 0;
		gbcCenter.gridy = 0;
		gbcCenter.gridwidth = 1;
		gbcCenter.gridheight = 1;
		gbcCenter.fill = GridBagConstraints.BOTH;

		layoutCenter.setConstraints(this.labelPath,gbcCenter);
		paneCenter.add(this.labelPath);

		gbcCenter.gridx = 1;
		layoutCenter.setConstraints(this.fieldPath,gbcCenter);
		paneCenter.add(this.fieldPath);

		Panel paneSouth = new Panel(new FlowLayout());
		paneSouth.add(this.buttonDone);

		super.add(paneCenter,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);
	}

	class Listener extends WindowAdapter implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			LoadDialog.this.result = LoadDialog.this.fieldPath.getText();
			LoadDialog.super.dispose();
		}

		@Override
		public void windowClosing(WindowEvent e)
		{
			LoadDialog.super.dispose();
		}
	}
}
