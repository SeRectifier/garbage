package mono.core;

import java.awt.image.*;

public final class Converter
{
	public static BufferedImage makeSquare(BufferedImage src,int size)
	{
		if(size < 1) return null;

		int widthSource = src.getWidth();
		int heightSource = src.getHeight();
		int scoreReduction = (widthSource < heightSource ? widthSource : heightSource) / size;
		int scoreEffective = scoreReduction * size;

		int xOffset = (widthSource - scoreEffective) / 2;
		int yOffset = (heightSource - scoreEffective) / 2;
		BufferedImage res = new BufferedImage(size,size,BufferedImage.TYPE_INT_ARGB);

		int argb = 0;
		long sumAlpha = 0;
		long sumRed = 0;
		long sumGreen = 0;
		long sumBlue = 0;

		for(int xRes=0; xRes<size; xRes++) {
		for(int yRes=0; yRes<size; yRes++) {
			for(int xSrc=0; xSrc<scoreReduction; xSrc++) {
			for(int ySrc=0; ySrc<scoreReduction; ySrc++) {
				argb = src.getRGB(
						xOffset + xRes*scoreReduction + xSrc,
						yOffset + yRes*scoreReduction + ySrc
				);
				sumAlpha += ((argb >> 24) & 0xff);
				sumRed += ((argb >> 16) & 0xff);
				sumGreen += ((argb >> 8) & 0xff);
				sumBlue += (argb & 0xff);
			}}

			argb = 
					((int) (sumAlpha / scoreReduction / scoreReduction) << 24) + 
					((int) (sumRed / scoreReduction / scoreReduction) << 16) + 
					((int) (sumGreen / scoreReduction / scoreReduction) << 8) +
					((int) (sumBlue / scoreReduction / scoreReduction));

			sumAlpha = 0;
			sumRed = 0;
			sumGreen = 0;
			sumBlue = 0;

			res.setRGB(xRes,yRes,argb);
		}}

		return res;
	}
}
