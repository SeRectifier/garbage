package mono.core.data;

import java.io.*;

public class ScoredCoordinate implements Serializable,Comparable<ScoredCoordinate>
{
	private static final long serialVersionUID = 0x0L;

	public int x = 0;
	public int y = 0;
	public int score = 0;

	public ScoredCoordinate() {}

	public ScoredCoordinate(int x,int y,int s)
	{
		this.x = x;
		this.y = y;
		this.score = s;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof ScoredCoordinate)) return false;
		ScoredCoordinate cdnt = (ScoredCoordinate) obj;

		return ((cdnt.x == this.x) && (cdnt.y == this.y) && (cdnt.score == this.score));
	}

	@Override
	public int compareTo(ScoredCoordinate sc)
	{
		return this.score - sc.score;
	}
}
