package mono.core.data;

import java.awt.image.*;
import java.io.*;

public final class Monochrome implements Serializable,Comparable<Monochrome>
{
	private static final long serialVersionUID = 0x0L;

	private static int DEFAULT_WIDTH = 64;
	private static int DEFAULT_HEIGHT = 64;

	private int width = 0;
	private int height = 0;
	private int total = 0;
	private int[][] grid = null;

	private int average = 0;

	public Monochrome()
	{
		this.initialize();
	}

	public Monochrome(BufferedImage image)
	{
		if(image == null) throw new IllegalArgumentException("Null image");

		this.width = image.getWidth();
		this.height = image.getHeight();
		this.total = this.width * this.height;
		this.grid = new int[this.width][this.height];

		long sum = 0;

		int argb = 0;
		int red = 0;
		int green = 0;
		int blue = 0;
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			argb = image.getRGB(x,y);
			red = (argb >> 16) & 0xff;
			green = (argb >> 8) & 0xff;
			blue = argb & 0xff;
			this.grid[x][y] = Math.round((red + green + blue) / 3.0f);
			sum += this.grid[x][y];
		}}

		this.average = (int) (sum / this.total);
	}

	@Override
	public int compareTo(Monochrome m)
	{
		return this.getAverage() - m.getAverage();
	}

	private void calcAverage()
	{
		long sum = 0;

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			sum += grid[x][y];
		}}

		this.average = (int) (sum / this.total);
	}

	public void setWidth(int w)
	{
		if(w < 1) throw new IllegalArgumentException("Invalid width");

		int widthOld = this.width;
		this.width = w;

		int[][] gridOld = this.grid;
		this.grid = new int[this.width][this.height];

		this.total = this.width * this.height;

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			if(x < widthOld) this.grid[x][y] = gridOld[x][y];
			else this.grid[x][y] = 0;
		}}

		this.calcAverage();
	}

	public void setHeight(int h)
	{
		if(h < 1) throw new IllegalArgumentException("Invalid height");

		int heightOld = this.height;
		this.height = h;

		int[][] gridOld = this.grid;
		this.grid = new int[this.width][this.height];

		this.total = this.width * this.height;

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			if(y < heightOld) this.grid[x][y] = gridOld[x][y];
			else this.grid[x][y] = 0;
		}}

		this.calcAverage();
	}

	public void setBrightnessAt(int x, int y, int b)
	{
		this.grid[x][y] = (b < 0 || 0xff < b) ? 0 : b;
		this.calcAverage();
	}

	public int getBrightnessAt(int x, int y)
	{
		return this.grid[x][y];
	}

	// Utility methods

	public void initialize()
	{
		this.width = Monochrome.DEFAULT_WIDTH;
		this.height = Monochrome.DEFAULT_HEIGHT;
		this.total = this.width * this.height;
		this.grid = new int[this.width][this.height];

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = 0;
		}}

		this.average = 0;
	}

	public void initialize(int w,int h)
	{
		if(w < 1 || h < 1) throw new IllegalArgumentException("Invalid size");

		this.width = w;
		this.height = h;
		this.total = this.width * this.height;
		this.grid = new int[this.width][this.height];

		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			this.grid[x][y] = 0;
		}}

		this.average = 0;
	}

	public void setSize(int w, int h)
	{
		this.setWidth(w);
		this.setHeight(h);
	}

	public int getAverage()
	{
		return this.average;
	}

	public BufferedImage toImage()
	{
		int value = 0;
		BufferedImage image = new BufferedImage(this.width,this.height,BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<this.width; x++) {
		for(int y=0; y<this.height; y++) {
			value = this.grid[x][y];
			image.setRGB(x,y,(value << 16) + (value << 8) + value + 0xff000000);
		}}

		return image;
	}
}
