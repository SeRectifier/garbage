package length.gui;

import java.awt.*;
import java.awt.event.*;

public class Master extends Frame
{
	private static final long serialVersionUID = 0x0L;

	private final Label labelCount = new Label();
	private final Label labelGoal = new Label();
	private final Label labelDifference = new Label();
	private final Label labelSpace = new Label();
	private final Label labelTab = new Label();
	private final Label labelLine = new Label();
	private final TextField field = new TextField();
	private final TextArea area = new TextArea();
	private final Button buttonSet = new Button("Set goal");
	private final Button buttonQuit = new Button("Quit");

	private int count = 0;
	private int goal = 0;
	private int difference = 0;
	private int space = 0;
	private int tab = 0;
	private int line = 0;

	public Master()
	{
		super("Length");

		Master.this.updateInfo();

		this.initField();
		this.initListener();
		this.initPane();

		super.setSize(141*5,100*5);
		super.setResizable(true);
	}

	public void updateInfo()
	{
		this.difference = this.goal - this.count;

		this.labelCount.setText("Count: " + this.count);
		this.labelGoal.setText("Goal: " + this.goal);
		this.labelDifference.setText("Difference: " + this.difference);
		this.labelSpace.setText("Spaces: " + this.space);
		this.labelTab.setText("Tabs: " + this.tab);
		this.labelLine.setText("Lines: " + this.line);

		if(this.difference == 0) this.labelCount.setForeground(Color.BLACK);
		else if(this.difference < 0) this.labelCount.setForeground(Color.BLUE);
		else this.labelCount.setForeground(Color.RED);
	}

	private void initField()
	{
		Label[] arrayLabel = new Label[] {
				this.labelCount,
				this.labelGoal,
				this.labelDifference,
				this.labelSpace,
				this.labelTab,
				this.labelLine,
		};

		Font font = new Font("Info",Font.BOLD,16);

		for(int i=0; i<arrayLabel.length; i++)
			arrayLabel[i].setFont(font);
	}

	private void initListener()
	{
		TextAreaListener listenerTextArea = new TextAreaListener();
		QuitListener listenerQuit = new QuitListener();

		this.area.addTextListener(listenerTextArea);
		this.buttonSet.addActionListener(listenerTextArea);
		this.buttonQuit.addActionListener(listenerQuit);
		super.addWindowListener(listenerQuit);
	}

	private void initPane()
	{
		GridBagLayout layoutNorth = new GridBagLayout();
		GridBagConstraints gbcNorth = new GridBagConstraints();
		Panel paneNorth = new Panel(layoutNorth);

		gbcNorth.weightx = 1;
		gbcNorth.gridx = 0;
		gbcNorth.gridy = 0;
		gbcNorth.gridwidth = 1;
		gbcNorth.gridheight = 1;
		gbcNorth.fill = GridBagConstraints.BOTH;

		Component[] arrayComp = new Component[] {
				this.labelGoal,
				this.labelCount,
				this.labelDifference,
				this.labelSpace,
				this.labelTab,
				this.labelLine,
		};

		for(int i=0; i<arrayComp.length; i++) {
			gbcNorth.gridx = i%3;
			gbcNorth.gridy = i/3;
			layoutNorth.setConstraints(arrayComp[i],gbcNorth);
			paneNorth.add(arrayComp[i]);
		}

		gbcNorth.gridx = 0;
		gbcNorth.gridy = 2;
		gbcNorth.gridwidth = 2;
		layoutNorth.setConstraints(this.field,gbcNorth);
		paneNorth.add(this.field);

		gbcNorth.gridx = 2;
		gbcNorth.gridwidth = 1;
		layoutNorth.setConstraints(this.buttonSet,gbcNorth);
		paneNorth.add(this.buttonSet);

		FlowLayout layoutSouth = new FlowLayout();
		Panel paneSouth = new Panel(layoutSouth);
		paneSouth.add(this.buttonQuit);

		super.add(paneNorth,BorderLayout.NORTH);
		super.add(this.area,BorderLayout.CENTER);
		super.add(paneSouth,BorderLayout.SOUTH);
	}

	class TextAreaListener implements TextListener,ActionListener
	{
		@Override
		public void textValueChanged(TextEvent e)
		{
			String text = Master.this.area.getText();
			Master.this.count = text.length();

			char space = ' ';
			char tab = '\t';
			char lf = '\n';
			int countSpace = 0;
			int countTab = 0;
			int countLine = 0;
			for(int i=0; i<text.length(); i++) {
				if(text.charAt(i) == space) countSpace++;
				if(text.charAt(i) == tab) countTab++;
				if(text.charAt(i) == lf) countLine++;
			}

			Master.this.space = countSpace;
			Master.this.tab = countTab;
			Master.this.line = countLine;
			Master.this.updateInfo();
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			try {
				int goalNew = Integer.parseInt(Master.this.field.getText());
				if(goalNew < 0) return;
				Master.this.goal = goalNew;
				Master.this.updateInfo();
			}
			catch(NumberFormatException nfe) {}
		}
	}

	class QuitListener extends WindowAdapter implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}

		@Override
		public void windowClosing(WindowEvent e)
		{
			System.exit(0);
		}
	}
}
